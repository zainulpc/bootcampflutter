import 'package:flutter/material.dart';
import 'Tugas/Tugas11/HomeScreen.dart';
import 'Tugas/Tugas11/LoginScreen.dart';

void main() {
  runApp(MaterialApp(
    initialRoute: '/',
    routes: <String, WidgetBuilder>{
      '/': (context) => LoginScreen(),
      '/home': (context) => HomeScreen(),
    },
  ));
}
