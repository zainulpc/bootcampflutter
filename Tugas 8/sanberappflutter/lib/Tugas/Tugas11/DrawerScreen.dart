import 'package:flutter/material.dart';

class DrawerScreen extends StatefulWidget {
  @override
  _DrawwerScreenState createState() => _DrawwerScreenState();
}

class _DrawwerScreenState extends State<DrawerScreen> {
  @override
  Widget build(BuildContext context) {
    return Drawer(
        child: ListView(
      children: <Widget>[
        UserAccountsDrawerHeader(
          accountName: Text("Zainullah"),
          currentAccountPicture: CircleAvatar(
              backgroundImage: AssetImage("assets/img/zainul.jpg")),
          accountEmail: Text("zainulzeitcom@gmail.com"),
        ),
        DrawerListTitle(
          iconData: Icons.group,
          title: "News Group",
          onTilePressed: () {},
        ),
        DrawerListTitle(
          iconData: Icons.group,
          title: "News Secret Group",
          onTilePressed: () {},
        ),
        DrawerListTitle(
          iconData: Icons.group,
          title: "News Channel Chat",
          onTilePressed: () {},
        ),
        DrawerListTitle(
          iconData: Icons.group,
          title: "Contacts",
          onTilePressed: () {},
        ),
        DrawerListTitle(
          iconData: Icons.group,
          title: "Saved Massage",
          onTilePressed: () {},
        ),
        DrawerListTitle(
          iconData: Icons.group,
          title: "Call",
          onTilePressed: () {},
        )
      ],
    ));
  }
}

class DrawerListTitle extends StatelessWidget {
  final IconData? iconData;
  final String? title;
  final VoidCallback? onTilePressed;

  const DrawerListTitle(
      {Key? key, this.iconData, this.title, this.onTilePressed})
      : super(key: key);
  @override
  Widget build(BuildContext context) {
    return ListTile(
      onTap: onTilePressed,
      dense: true,
      leading: Icon(iconData),
      title: Text(
        title!,
        style: TextStyle(fontSize: 16),
      ),
    );
  }
}
