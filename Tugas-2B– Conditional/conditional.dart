import 'dart:io';
void main(){
    print("1. Ternary operator ");
    print("-------------------------");
    print("Apakah yakin melanjutkan instal aplikasi ?y/t ");
    print("Jawab : ");
    var jawaban = stdin.readLineSync();
    var output = (jawaban == 'y') ? 'anda akan menginstall aplikasi dart' : 'aborted';
    print(output);

    print("");
    print("2. If-else if dan else");
    print("-------------------------");
    print("Selamat datang dunia game werewolf ");
    print("Isi Nama : ");
    var nama = stdin.readLineSync();
    print("Isi Peran : Penyihir/Guard/Werewolf");
    var peran = stdin.readLineSync();
    
    if(nama==''){
        print('Nama harus diisi..!!!');
        print("Isi Nama : ");
        var peran = stdin.readLineSync();
    }else{
        if(peran==''){
            print("Pilih Peranmu untuk memulai game");
            print("Isi Peran : Penyihir/Guard/Werewolf ");
            var peran = stdin.readLineSync();
        }else{  
            if(peran=='Penyihir'){
                print("Selamat datang di Dunia Werewolf, $nama");
                print("Halo Penyihir $nama, kamu dapat melihat siapa yang menjadi werewolf!");
            }else if(peran=='Guard'){
                print("Selamat datang di Dunia Werewolf, $nama");
                print("Halo Guard $nama, kamu akan membantu melindungi temanmu dari serangan werewolf.");
            }else{
                print("Selamat datang di Dunia Werewolf, $nama");
                print("Halo Werewolf $nama, Kamu akan memakan mangsa setiap malam!");
            }

        }
    }

    print("");
    print("3. Switch case");
    print("-------------------------");
    print("Ketik hari ?: ");
    var hariini = stdin.readLineSync();
      switch(hariini) { 
      case "Senin": {  print("Segala sesuatu memiliki kesudahan, yang sudah berakhir biarlah berlalu dan yakinlah semua akan baik-baik saja."); } 
      break; 
     
      case "Selasa": {  print("Setiap detik sangatlah berharga karena waktu mengetahui banyak hal, termasuk rahasia hati."); } 
      break; 
     
      case "Rabu": {  print("Jika kamu tak menemukan buku yang kamu cari di rak, maka tulislah sendiri."); } 
      break; 
     
      case "Kamis": {  print("Jika hatimu banyak merasakan sakit, maka belajarlah dari rasa sakit itu untuk tidak memberikan rasa sakit pada orang lain."); } 
      break;

      case "Jum'at": {  print("Hidup tak selamanya tentang pacar."); } 
      break; 

      case "Sabtu": {  print("Rumah bukan hanya sebuah tempat, tetapi itu adalah perasaan."); } 
      break; 
     
      case "Ahad": {  print("Hanya seseorang yang takut yang bisa bertindak berani. Tanpa rasa takut itu tidak ada apapun yang bisa disebut berani."); } 
      break; 
      default: { print("hari invalid"); } 
      break; 
   } 

    print("");
    print("4. Switch Case");
    print("-------------------------");
    var tanggal = 21; 
    var bulan = 1; 
    var tahun = 1945;
    //int bln = int.parse(bulan!);

  switch (bulan) {
    case 1:
      print ('$tanggal Januari $tahun');
      break;

    case 2:
      print ('$tanggal Februari $tahun');
      break;

    case 3:
      print ('$tanggal Maret $tahun');
      break;

    case 4:
     print ('$tanggal April $tahun');
      break;

    case 5:
      print ('$tanggal Mei $tahun');
      break;

    case 6:
      print ('$tanggal Juni $tahun');
      break;
    case 7:
     print ('$tanggal Juli  $tahun');
      break;
    case 8:
     print ('$tanggal Agustus $tahun');
      break;
      case 9:
     print ('$tanggal September $tahun');
      break;
      case 10:
     print ('$tanggal Oktober $tahun');
      break;
      case 11:
     print ('$tanggal November $tahun');
      break;
      case 12:
    print ('$tanggal Desember $tahun');
      break;
    default:
      print(' invalid entry');
  }

}