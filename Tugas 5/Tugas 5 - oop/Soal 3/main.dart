import 'armor_titan.dart';
import 'attack_titan.dart';
import 'beast_titan.dart';
import 'human.dart';
import 'titan.dart';

void main() {
  ArmorTitan armorTitan = ArmorTitan(0);
  AttackTitan attackTitan = AttackTitan(0);
  BeastTitan beastTitan = BeastTitan(0);
  Human human = Human(0);

  armorTitan.powerPoint = 3;
  attackTitan.powerPoint = 8;
  beastTitan.powerPoint = 6;
  human.powerPoint = 10;

  print("Armor Titan Power Point: ${armorTitan.powerPoint}");
  print("Attack Titan Power Point: ${attackTitan.powerPoint}");
  print("Beast Titan Power Point: ${beastTitan.powerPoint}");
  print("Human Power Point: ${human.powerPoint}");

  print("Armor Titan: ${armorTitan.terjang()}");
  print("Attack Titan: ${attackTitan.punch()}");
  print("Beast Titan: ${beastTitan.lempar()}");
  print("Human: ${human.killAllTitan()}");
}
