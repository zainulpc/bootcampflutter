class Lingkaran {
  double _radius = 0;

  Lingkaran(double radius) {
    this.radius = radius;
  }

  set radius(double value) {
    if (value >= 0) {
      _radius = value;
    }
  }

  double get radius => _radius;
  get area => radius * radius * 3.14;
}
