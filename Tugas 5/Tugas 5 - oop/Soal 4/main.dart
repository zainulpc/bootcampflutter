import 'employee.dart';

void main() {
  Employee employee1 = Employee(001, "ZAINUL", "TATA USAHA");
  Employee employee2 = Employee(002, "RIFKI", "BENDAHARA");

  print("iNFORMASI:");
  employee1.displayInfo();
  employee2.displayInfo();
}
