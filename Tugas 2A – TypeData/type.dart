import 'dart:io';
void main(List<String>args){
    print("1. Soal No. 1 (Membuat kalimat),");
    print("-------------------------------------");
    var word = 'dart';
    var second = 'is';
    var third = 'awesome';
    var fourth = 'and';
    var fifth = 'I';
    var sixth = 'love';
    var seventh = 'it!'; 
         //Buatlah agar kata-kata di atas menjadi satu kalimat . Output: Dart is awesome and I love it!
        print(word +' '+ second +' '+ third +' '+ fourth +' '+ fifth+' '+ sixth +' '+ seventh );

    print(" ");
    print("2. Soal No.2 Mengurai kalimat (Akses karakter dalam string)");
    print("-------------------------------------");
    var sentence = "I am going to be Flutter Developer";
    var exampleFirstWord = sentence[0] ;
    var secondWord = sentence[2] + sentence[3] ;
    var thirdWord =sentence[5] + sentence[6] + sentence[7]+sentence[8]+sentence[9];
    var fourthWord= sentence[11] + sentence[12] ;
    var fifthWord= sentence[14] + sentence[15] ;
    var sixthWord= sentence[17] + sentence[18] + sentence[19] + sentence[20]+ sentence[21] + sentence[22] + sentence[23] ; 
    var seventhWord= sentence[25] + sentence[26]+ sentence[27]+ sentence[28]+ sentence[29]+ sentence[30]+ sentence[31]+ sentence[32]; 


    print('First Word: ' + exampleFirstWord);
    print('Second Word: ' + secondWord);
    print('Third Word: ' + thirdWord);
    print('Fourth Word: ' + fourthWord);
    print('Fifth Word: ' + fifthWord);
    print('Sixth Word: ' + sixthWord);
    print('Seventh Word: ' + seventhWord);
    
    print(" ");
    print("3. Dengan menggunakan I/O pada dart buatlah input dinamis yang akan menginput nama depan dan belakang dan jika di enter");
    print("-------------------------------------");
    print("masukan nama depan :");
    String? namaDepan = stdin.readLineSync()!;
    print("Masukkan nama belakang");
    String? namaBelakang = stdin.readLineSync()!;
    print("Nama : ${namaDepan} ${namaBelakang}");

    print(" ");
    print("4. Dengan menggunakan operator operasikan variable berikut ini menjadi bentuk operasi perkalian, penjumlahan, pengurangan dan pembagian a = 5,  b = 10 jadi misal a * b = 5 * 10 = 50 dst.");
    print("-------------------------------------");
    int a = 5,  b = 10;
    int perkalian = a * b;
    double  pembagian = a / b;
    int penambahan = a + b;
    int pengurangan = a - b;
    print('Perkalian : $perkalian');
    print('Pembagian : $pembagian');
    print('Penambahan : $penambahan');
    print('Pengurangan : $pengurangan');

}
