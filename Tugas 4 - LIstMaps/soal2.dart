void main() {
  print("Soal NO. 2 Range");
  print("----------------------");
  print(range(1, 10, 2));
  print(range(11, 23, 3));
  print(range(5, 2, 1));
  // print();
}

range(startNum, finishNum, step) {
  if (startNum < finishNum) {
    var list = [for (var i = startNum; i <= finishNum; i += step) i];
    return list;
    // return "menurun";
  } else {
    var list = [for (var i = startNum; i >= finishNum; i -= step) i];
    return list;
    // return "naik";
  }
}
