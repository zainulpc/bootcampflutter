import 'dart:io';

void main() {
  print("No. 1 Looping While ");
  print("--------------------");
  print(" ");
  print("LOOPING PERTAMA");
  int numberPertama = 1;
  while (numberPertama <= 20) {
    if (numberPertama % 2 == 0) {
      print('$numberPertama - I love coding');
    }
    numberPertama++;
  }
  print("LOOPING KEDUA");
  int numberKedua = 20;
  while (numberKedua > 1) {
    if (numberKedua % 2 == 0) {
      print('$numberKedua - I will become a mobile developer');
    }
    numberKedua--;
  }
  print(" ");
  print("No. 2 Looping menggunakan for ");
  print("--------------------");
  int nfor;
  for (nfor = 1; nfor <= 20; nfor++) {
    if (nfor % 2 == 0) {
      print('$nfor Berkualitas ');
    } else if (nfor % 3 == 0) {
      print('$nfor I Love Coding  ');
    } else {
      print('$nfor Santai ');
    }
    //print(nfor);
  }
  print(" ");
  print("No. 3 Membuat Persegi Panjang #");
  print("--------------------");
  int samping = 8;
  int bawah = 4;
  for (int i = 0; i < bawah; i++) {
    for (int j = 0; j < samping; j++) {
      stdout.write('#');
    }
    stdout.writeln();
  }
  print(" ");
  print("No. 4 Membuat Tangga ");
  print("--------------------");
  for (int i = 0; i < 7; i++) {
    for (int j = 0; j <= i; j++) {
      stdout.write('#');
    }
    stdout.writeln();
  }
}
