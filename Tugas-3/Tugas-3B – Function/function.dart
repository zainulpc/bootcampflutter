import 'dart:io';

void main() {
  print('No. 1 ');
  print('---------------');
  print(teriak());

  print(' ');
  print('No. 2 ');
  print('---------------');

  var num1 = 12;
  var num2 = 4;

  var hasilKali = kalikan(num1, num2);
  print(hasilKali); // 48

  print(' ');
  print('No. 3 ');
  print('---------------');
  var name = "Agus";
  var age = 30;
  var address = "Jln. Malioboro, Yogyakarta";
  var hobby = "Gaming";
  print(introduce(name, age, address, hobby));

  print(' ');
  print('No. 4 ');
  print('---------------');
  int fak = 6;
  print(faktorial(fak));
}

//function Faktorial
faktorial(x) {
  var fak = x;
  int result = 1;
  if (fak <= 0) {
    return ('Hasil faktorial $fak! = $result');
  } else {
    for (int i = 1; i <= fak; i++) {
      result *= i;
    }
    return ('Hasil faktorial $fak! = $result');
  }
}

//function introduce
introduce(a, b, c, d) {
  return 'Nama saya $a, umur saya $b tahun, alamat saya di $c dan saya punya hobby yaitu $d!';
}

//fanction teriak
teriak() {
  return ("Halo Sanbers!");
}

//fanction kalikan
kalikan(x, y) {
  return x * y;
}
