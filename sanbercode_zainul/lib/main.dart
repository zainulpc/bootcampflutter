import 'package:flutter/material.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:sanbercode_zainul/flashscreen.dart';
import 'firebase_options.dart';
//import 'login_page.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp(
    options: DefaultFirebaseOptions.currentPlatform,
  );
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Simple Login App',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: SplashScreen(),
    );
  }
}
// class MyApp extends StatelessWidget {
//   const MyApp({super.key});

//   // This widget is the root of your application.
//     @override
//   Widget build(BuildContext context) {
//     return MaterialApp(
//       title: 'Flutter Demo',
//       debugShowCheckedModeBanner: false,
//       theme: ThemeData(
//         fontFamily: 'nsb',
//         scaffoldBackgroundColor: Color.fromARGB(255, 255, 255, 255),
//         appBarTheme: const AppBarTheme(
//           backgroundColor: Color.fromARGB(255, 30, 49, 157),
//         ),
//       ),
//       home: const SplashScreen(),
//     );
//   }
// }


