import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

class QuranListPage extends StatefulWidget {
  @override
  _QuranListPageState createState() => _QuranListPageState();
}

class _QuranListPageState extends State<QuranListPage> {
  late List<dynamic> surahs;

  @override
  void initState() {
    super.initState();
    fetchData();
  }

  Future<void> fetchData() async {
    final response =
        await http.get(Uri.parse('https://api.quran.sutanlab.id/surah'));

    if (response.statusCode == 200) {
      final Map<String, dynamic> data = json.decode(response.body);
      setState(() {
        surahs = data['data'];
      });
    } else {
      throw Exception('Failed to load data');
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Al-Qur\'an Surahs'),
      ),
      body: surahs != null
          ? ListView.builder(
              itemCount: surahs.length,
              itemBuilder: (context, index) {
                return Card(
                  margin: EdgeInsets.all(8.0),
                  child: ListTile(
                    title: Text(
                      surahs[index]['name'],
                      style: TextStyle(fontWeight: FontWeight.bold),
                    ),
                    subtitle: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text('Ayat Count: ${surahs[index]['numberOfAyahs']}'),
                        Text(
                            'Revelation Type: ${surahs[index]['revelationType']}'),
                      ],
                    ),
                    onTap: () {
                      // Navigasi ke halaman detail surah
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (context) =>
                              SurahDetailPage(surahData: surahs[index]),
                        ),
                      );
                    },
                  ),
                );
              },
            )
          : Center(
              child: CircularProgressIndicator(),
            ),
    );
  }
}

class SurahDetailPage extends StatelessWidget {
  final dynamic surahData;

  SurahDetailPage({required this.surahData});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(surahData['name']),
      ),
      body: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text('Ayat Count: ${surahData['numberOfAyahs']}'),
            Text('Revelation Type: ${surahData['revelationType']}'),
            // Tambahkan informasi atau widget lain sesuai kebutuhan
          ],
        ),
      ),
    );
  }
}
