import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:sanbercode_zainul/about_page.dart';
import 'surah_detail_page.dart';
import 'about_page.dart';

class MyHomePage extends StatefulWidget {
  const MyHomePage({super.key});

  @override
  // ignore: library_private_types_in_public_api
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  late List<dynamic> surahs;

  @override
  void initState() {
    super.initState();
    fetchData();
  }

  Future<void> fetchData() async {
    final response =
        await http.get(Uri.parse('https://equran.id/api/v2/surat'));

    if (response.statusCode == 200) {
      final Map<String, dynamic> data = json.decode(response.body);
      setState(() {
        surahs = data['data'];
      });
    } else {
      throw Exception('Failed to load data');
    }
  }

  int _currentIndex = 0;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Aplikasi Al-Quran'),
        actions: [
          IconButton(
            icon: Icon(Icons.account_circle),
            onPressed: () {
              // Tampilkan menu pengaturan atau navigasikan ke halaman Tentang Saya
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => AboutPage(),
                ),
              );
            },
          ),
        ],
      ),
      body: Column(
        children: [
          // Widget banner gambar
          Image(
            image: AssetImage("asset/logo.jpg"),
            width: 400,
          ),
          // Widget daftar surah
          Expanded(
            // ignore: unnecessary_null_comparison
            child: surahs != null
                ? ListView.builder(
                    itemCount: surahs.length,
                    itemBuilder: (context, index) {
                      return Card(
                        margin: EdgeInsets.all(8.0),
                        child: ListTile(
                          title: Text(
                            surahs[index]['namaLatin'] +
                                ' | ' +
                                surahs[index]['nama'] +
                                '(' +
                                surahs[index]['arti'] +
                                ')',
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                          subtitle: Text(
                              'Jumlah Ayat: ${surahs[index]['jumlahAyat']} \n Tempat Turun: ${surahs[index]['tempatTurun']}'),
                          leading: CircleAvatar(
                            child: Text(
                                '${surahs[index]['nomor']}', // ambil karakter pertama text
                                style: TextStyle(fontSize: 20)),
                          ),
                          onTap: () {
                            // Navigasi ke halaman detail surah
                            Navigator.push(
                              context,
                              MaterialPageRoute(
                                builder: (context) => SurahDetailPage(
                                    surahNumber: surahs[index]['nomor']),
                              ),
                            );
                          },
                        ),
                      );
                    },
                  )
                : Center(
                    child: CircularProgressIndicator(),
                  ),
          ),
        ],
      ),
    );
  }
}
