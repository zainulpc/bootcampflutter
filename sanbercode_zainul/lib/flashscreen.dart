import 'dart:async';
import 'package:sanbercode_zainul/login_page.dart';
//import 'package:sanbercode_zainul/flashscreen.dart';
import 'package:flutter/material.dart';

class SplashScreen extends StatefulWidget {
  const SplashScreen({super.key});

  @override
  State<SplashScreen> createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  @override
  void initState() {
    super.initState();
    Timer(const Duration(seconds: 5), () {
      Navigator.of(context).pushReplacement(MaterialPageRoute(
        builder: (context) => LoginPage(),
      ));
    });
  }

  @override
  Widget build(BuildContext context) {
    return const Scaffold(
      backgroundColor: Color.fromARGB(255, 30, 49, 157),
      body: Center(
        child: Image(
          image: AssetImage("asset/logosmausa.png"),
          width: 150,
        ),
      ),
    );
  }
}
