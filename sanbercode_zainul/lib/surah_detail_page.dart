// surah_detail_page.dart
import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

class SurahDetailPage extends StatefulWidget {
  final int surahNumber;

  SurahDetailPage({required this.surahNumber});

  @override
  _SurahDetailPageState createState() => _SurahDetailPageState();
}

class _SurahDetailPageState extends State<SurahDetailPage> {
  late Future<dynamic> surahData;

  @override
  void initState() {
    super.initState();
    surahData = fetchSurahData(widget.surahNumber);
  }

  Future<dynamic> fetchSurahData(int surahNumber) async {
    final response = await http
        .get(Uri.parse('https://equran.id/api/v2/surat/$surahNumber'));

    if (response.statusCode == 200) {
      final Map<String, dynamic> data = json.decode(response.body);
      return data['data'];
    } else {
      throw Exception('Gagal memuat data surah');
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Detail Surah'),
      ),
      body: FutureBuilder(
        future: surahData,
        builder: (context, snapshot) {
          if (snapshot.connectionState == ConnectionState.waiting) {
            return Center(
              child: CircularProgressIndicator(),
            );
          } else if (snapshot.hasError || snapshot.data == null) {
            return Center(
              child: Text('Gagal memuat data surah'),
            );
          } else {
            final dynamic data = snapshot.data;

            if (data != null && data['ayat'] != null) {
              final List<dynamic> arrayData = data['ayat'];

              return ListView.builder(
                itemCount: arrayData.length,
                itemBuilder: (context, index) {
                  return ListTile(
                    title: Text(
                        'Ayat ' + arrayData[index]['nomorAyat'].toString()),
                    subtitle: Text(
                      '${arrayData[index]['teksArab'].toString()}',
                      style: TextStyle(fontSize: 30),
                      textAlign: TextAlign.right,
                    ),
                    // Tambahkan informasi atau widget lain sesuai kebutuhan
                  );
                },
              );
            } else {
              return Center(
                child: Text('Data array kosong atau tidak tersedia.'),
              );
            }
          }
        },
      ),
    );
  }
}
