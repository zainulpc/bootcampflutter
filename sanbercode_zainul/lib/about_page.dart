// about_page.dart
import 'package:flutter/material.dart';
import 'package:sanbercode_zainul/home.dart';
import 'package:url_launcher/url_launcher.dart';

class AboutPage extends StatelessWidget {
  void _openWhatsAppChat() async {
    final phoneNumber =
        '6282338621497'; // Ganti dengan nomor WhatsApp yang diinginkan
    final url = 'https://wa.me/$phoneNumber';

    if (await canLaunch(url)) {
      await launch(url);
    } else {
      print('Tidak dapat membuka WhatsApp.');
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Tentang Saya'),
      ),
      body: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            CircleAvatar(
              radius: 50,
              backgroundImage: AssetImage(
                  'asset/fotoku.jpg'), // Ganti dengan path gambar Anda
            ),
            SizedBox(height: 16),
            Text(
              'ZAINULLAH',
              style: TextStyle(fontSize: 24, fontWeight: FontWeight.bold),
            ),
            SizedBox(height: 8),
            Text(
              'Saya web programing  otodidak. Selama ini mencoba belajar program dengan mencari referensi melalui internet, \n Saat ini mencoba belajar bersama SunberCode .',
              textAlign: TextAlign.center,
            ),
            SizedBox(height: 16),
            ElevatedButton(
              onPressed: _openWhatsAppChat,
              child: Text('Hubungi via WhatsApp'),
            ),
          ],
        ),
      ),
    );
  }
}
